def project_id     = '42694451'
def project_url    = 'https://gitlab.com/vuthanhtb.dev/react-admin-dashboard-2.git'
def branch_default = '*/master'
def commitStatus   = 'build'

def namespace      = 'admin-dashboard'
def service_name   = 'react-admin-dashboard'

def aws_acc_num    = '123456789'
def region_name    = 'ap-southeast-1'
def image_tag      = 'latest'
def image_name     = aws_acc_num + '.dkr.ecr.' + region_name + '.amazonaws.com/frontend-' + service_name + ':' + image_tag

def k8s_config_name = 'k8s-config.yaml'
def credential_git  = 'credential_git'

pipeline {
  agent any
  options {
    gitLabConnection('gitlab')
    skipDefaultCheckout(true)
  }
  tools {
    jdk 'jdk17'
    nodejs 'node16'
  }
  environment {
    SCANNER_HOME=tool 'sonar-scanner'
  }
  parameters {
    gitParameter name: 'branch_name',
                  type: 'PT_BRANCH_TAG',
                  defaultValue: branch_default,
                  useRepository: project_url
  }
  stages {
    stage('1. Checkout') {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          cleanWs()
          checkout([
            $class: 'GitSCM',
            branches: [[name: branch_name]],
            extensions: [],
            userRemoteConfigs: [[credentialsId: credential_git, url: project_url]]
          ])
        }
      }
    }
    stage("2. Sonarqube Analysis") {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          script {
            withSonarQubeEnv('sonar-server') {
              sh ''' $SCANNER_HOME/bin/sonar-scanner -Dsonar.projectName=react-admin-dashboard -Dsonar.projectKey=react-admin-dashboard '''
            }
          }
        } 
      }
    }
    stage("3. Quality Gate") {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          script {
            waitForQualityGate abortPipeline: false, credentialsId: 'sonar-token' 
          }
        }
      } 
    }
    stage('4. Build SourceCode') {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          sh '''#!/bin/bash
          node -v
          npm install
          npm run build
          '''
        }
      }
    }
    stage('5. OWASP FS SCAN') {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          dependencyCheck additionalArguments: '--scan ./ --disableYarnAudit --disableNodeAudit', odcInstallation: 'DP-Check'
          dependencyCheckPublisher pattern: '**/dependency-check-report.xml'
        }
      }
    }
    stage('6. TRIVY FS SCAN') {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          sh 'trivy fs . > trivyfs.txt'
        }
      }
    }
    stage("7. Push Artifact") {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          sh '''#!/bin/bash
          sudo docker build -t''' + image_name + ''' .
          sudo docker images
          '''
          // sh 'aws ecr get-login-password --region ${region_name} | docker login --username AWS --password-stdin ${aws_acc_num}.dkr.ecr.${region_name}.amazonaws.com'
          // sh 'docker push ${image_name}'
        }
      }
    }
    stage("8. TRIVY") {
      steps {
        gitlabCommitStatus(name: commitStatus, builds: [[projectId: project_id, revisionHash: branch_name]]) {
          sh '''trivy image ''' + image_name + ''' > trivy.txt'''
        }
      }
    }
    // stage('9. Deploy') {
    //   steps {
    //     gitlabCommitStatus(name: commitStatus,  builds: [[projectId: project_id, revisionHash: branch_name]]) {
    //       sh '/usr/local/bin/kubectl apply -f ${k8s_config_name}'
    //       script {
    //         final String response = sh(script: '/usr/local/bin/kubectl get pod -l app=${service_name} -n ${namespace} --field-selector=status.phase==Pending --output name | wc -l', returnStdout: true)
    //         echo 'Number of pending pod: ${response}';
    //         if ("0".equals(response.trim())) {
    //           echo 'No new config'
    //           sh '/usr/local/bin/kubectl rollout restart deployment ${service_name} -n ${namespace}'
    //           sh '/usr/local/bin/kubectl rollout status deployment ${service_name} -n ${namespace}'
    //         } else {
    //           echo "Have new config - Apply Done"
    //           sh '/usr/local/bin/kubectl rollout status deployment ${service_name} -n ${namespace}'
    //         }
    //       }
    //     }
    //   }
    // }
  }
}
